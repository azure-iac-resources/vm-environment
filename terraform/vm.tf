module "azurerm_vnet" {
    source = "git::https://gitlab.com/azure-iac2/root-modules.git//vnet"    
    business_divsion    = var.business_divsion
    environment         = var.environment
    vnet_name           = var.vnet_name
    address_space       = var.address_space
    resource_group_name = data.azurerm_resource_group.rg.name
    location            = data.azurerm_resource_group.rg.location
}

# Create subnet
module "subnet" {
  source = "git::https://gitlab.com/azure-iac2/root-modules.git//subnet"
  subnet_name          = var.subnet_name
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  vnet_name            = "${var.business_divsion}-${var.environment}-${var.vnet_name}"
  sn_address_space     = var.sn_address_space

  depends_on = [module.azurerm_vnet]
}

# Create public ip
module "public_ip" {
  source = "git::https://gitlab.com/azure-iac2/root-modules.git//public_ip"
  public_ip_name       = var.public_ip_name  
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  location             = data.azurerm_resource_group.rg.location
  allocation_method    = var.allocation_method
}

data "azurerm_subnet" "gl_subnet" {
  name                 = "${var.business_divsion}-${var.environment}-${var.subnet_name}"
  virtual_network_name = "${var.business_divsion}-${var.environment}-${var.vnet_name}"
  resource_group_name  = data.azurerm_resource_group.rg.name

  depends_on = [module.subnet]
}

data "azurerm_public_ip" "gl_public_ip" {
  name                = "${var.business_divsion}-${var.environment}-${var.public_ip_name}"
  resource_group_name = data.azurerm_resource_group.rg.name

  depends_on = [module.public_ip]
}

# Create nic
module "nic" {
  source               = "git::https://gitlab.com/azure-iac2/root-modules.git//nic"
  nic_name             = var.nic_name  
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  location             = data.azurerm_resource_group.rg.location
  nic_ip_name          = var.nic_ip_name
  subnet_id            = data.azurerm_subnet.gl_subnet.id
  private_ip_address_allocation = var.private_ip_address_allocation
  public_ip_address_id = data.azurerm_public_ip.gl_public_ip.id

  depends_on = [data.azurerm_subnet.gl_subnet,
                data.azurerm_public_ip.gl_public_ip
               ]
}


# Create NSG
module "nsg" {
  source                  = "git::https://gitlab.com/azure-iac2/root-modules.git//nsg"
  nsg_name                = var.nsg_name  
  business_divsion        = var.business_divsion
  environment             = var.environment
  resource_group_name     = data.azurerm_resource_group.rg.name
  location                = data.azurerm_resource_group.rg.location
  nsg-security-rule-name  = var.nsg-security-rule-name
}

data "azurerm_network_security_group" "nsg" {
  name                = "${var.business_divsion}-${var.environment}-${var.nsg_name}"
  resource_group_name = data.azurerm_resource_group.rg.name

  depends_on = [module.nsg]
}

data "azurerm_network_interface" "nic"{
  name                = "${var.business_divsion}-${var.environment}-${var.nic_name}"
  resource_group_name = data.azurerm_resource_group.rg.name

  depends_on = [module.nic]
}

# Create NSG-Association
module "nsg-association" {
  source                    = "git::https://gitlab.com/azure-iac2/root-modules.git//nsg-association"
  network_interface_id      = data.azurerm_network_interface.nic.id
  network_security_group_id = data.azurerm_network_security_group.nsg.id

  depends_on = [data.azurerm_network_security_group.nsg,
                data.azurerm_network_interface.nic
               ]
}

# Create virtual machine
module "vm" {
  source = "git::https://gitlab.com/azure-iac2/root-modules.git//vm"
  vm_name              = var.vm_name  
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  location             = data.azurerm_resource_group.rg.location
  nic_id               = data.azurerm_network_interface.nic.id
  vm_size              = var.vm_size
  computer_name        = var.computer_name
  admin_username       = var.admin_username
  admin_password       = var.admin_password  

  depends_on = [data.azurerm_resource_group.rg,
                data.azurerm_network_interface.nic
               ]
}

data "azurerm_virtual_machine" "data_vm" {
  name                 = "${var.business_divsion}-${var.environment}-${var.vm_name}" 
  resource_group_name  = data.azurerm_resource_group.rg.name

  depends_on = [module.vm]
}

resource "azurerm_virtual_machine_extension" "vm_extention" {
  name                  = var.computer_name
  virtual_machine_id    = data.azurerm_virtual_machine.data_vm.id
  publisher             = "Microsoft.Azure.Extensions"
  type                  = "CustomScript"
  type_handler_version  = "2.0"

  settings = <<SETTINGS
    {
        "script": "${base64encode(templatefile("custom_script.sh", {
          vmname="${data.azurerm_virtual_machine.data_vm.name}"
        }))}"
    }
  SETTINGS

  tags =  {
    environment = var.environment
  }

  depends_on = [data.azurerm_virtual_machine.data_vm]
}
