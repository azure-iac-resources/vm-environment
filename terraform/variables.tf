# Generic Input Variables

# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = ""
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = ""
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = ""
}

variable "location" {
  description = "Resource Group location"
  type = string
  default = ""
}

## VNET details
variable "vnet_name" {
  type = string
  default = ""
}

variable "address_space" {
  description = "Vnet Address space"
  type = string
  default = ""
}

## Subnet details
variable "subnet_name" {
  description = "Vnet name"
  type = string
  default = ""
}

variable "sn_address_space" {
  description = "subnet Address space"
  type = string
  default = ""
}

## Public IP details
variable "public_ip_name" {
  description = "public ip name"
  type = string
  default = ""
}

variable "allocation_method" {
  description = "public ip allocation method"
  type = string
  default = ""
}

## NIC details
variable "nic_name" {
  description = "NIC Name"
  type = string
  default = ""
}

variable "nic_ip_name" {
  description = "nic ip configuration name"
  type = string
  default = ""
}

variable "private_ip_address_allocation" {
  description = "NIC private_ip_address_allocation"
  type = string
  default = ""
}

## NSG details
variable "nsg_name" {
  description = "NSG Name"
  type = string
  default = ""
}

variable "nsg-security-rule-name" {
  description = "nsg security rule name"
  type = string
  default = ""
}

## VM details
variable "vm_name" {
  description = "virtual machine name"
  type = string
  default = ""
}

variable "nic_id" {
  description = "NIC ID"
  type = string
  default = ""
}

variable "vm_size" {
  description = "VM Size"
  type = string
  default = ""
}

variable "computer_name" {
  description = "OS Computer name"
  type = string
  default = ""
}

variable "admin_username" {
  description = "OS computer admin name"
  type = string
  default = ""
}

variable "admin_password" {
  description = "OS Computer admin password"
  type = string
  default = ""
}
